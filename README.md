[
![Docs](https://img.shields.io/badge/read-docs-success)
](https://a.pirogov.pages.hzdr.de/crack-spotter)
[
![Test Coverage](https://a.pirogov.pages.hzdr.de/crack-spotter/main/coverage_badge.svg)
](https://a.pirogov.pages.hzdr.de/crack-spotter/main/coverage)

<!-- --8<-- [start:abstract] -->
# crack-spotter

----
**:warning: TODO: Complete project setup :construction:**

This is a Python project generated from the
[fair-python-cookiecutter](https://github.com/Materials-Data-Science-and-Informatics/fair-python-cookiecutter)
template.

To finalize the project setup, please complete the following steps:

- [ ] Inspect the generated project files and adjust them as needed
- [ ] Take care of the TODOs in some of the files
- [ ] Check that everything works for you locally (running tests, building documentation)
- [ ] Create and add an empty remote repository (GitHub/GitLab) and push to it
- [ ] Wait and check that the CI pipeline runs successfully
- [ ] Follow the instructions in the developer guide in `docs/dev_guide.md` to
    * setup the deployment of your project documentation (to GitLab/GitHub Pages)
    * setup the deployment of your project releases (to GitLab/GitHub/PyPI)
- [ ] Remove this section
----

TODO - add description

**:construction: TODO: Write a paragraph summarizing what this project is about.**

<!-- --8<-- [end:abstract] -->
<!-- --8<-- [start:quickstart] -->

## Installation

**:construction: TODO: check that the installation instructions work**

This project works with Python > 3.8.

```
$ pip install git+ssh://git@codebase.helmholtz.cloud/a.pirogov/crack-spotter.git
```

## Getting Started


**:construction: TODO: provide a minimal working example**

<!-- --8<-- [end:quickstart] -->

## Troubleshooting

### When I try installing the package, I get an `IndexError: list index out of range`

Make sure you have `pip` > 21.2 (see `pip --version`), older versions have a bug causing
this problem. If the installed version is older, you can upgrade it with
`pip install --upgrade pip` and then try again to install the package.

**You can find more information on using and contributing to this repository in the
[documentation](https://a.pirogov.pages.hzdr.de/crack-spotter/main).**

<!-- --8<-- [start:citation] -->

## How to Cite

If you want to cite this project in your scientific work,
please use the [citation file](https://citation-file-format.github.io/)
in the [repository](https://codebase.helmholtz.cloud/a.pirogov/crack-spotter/blob/main/CITATION.cff).

<!-- --8<-- [end:citation] -->
<!-- --8<-- [start:acknowledgements] -->

## Acknowledgements

We kindly thank all
[authors and contributors](https://a.pirogov.pages.hzdr.de/crack-spotter/latest/credits).

**:construction: TODO: relevant organizational acknowledgements (employers, funders)**

<!-- --8<-- [end:acknowledgements] -->
