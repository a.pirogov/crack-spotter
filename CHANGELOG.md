# Changelog

Here we provide notes that summarize the most important changes in each released version.

Please consult the changelog to inform yourself about breaking changes and security issues.

## [v0.1.0](https://codebase.helmholtz.cloud/a.pirogov/crack-spotter/tree/v0.1.0) <small>(2024-01-03)</small> { id="0.1.0" }

* First release

